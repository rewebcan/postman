<?php

namespace Tests\Unit\Models;

use App\Models\Mail;
use Illuminate\Foundation\Testing\WithFaker;
use Mockery;
use Tests\TestCase;

/**
 * Class MailTest
 * @package Tests\Unit\Models
 * @coversDefaultClass \App\Models\Mail
 */
class MailTest extends TestCase
{
    use WithFaker;

    /**
     * @test
     * @covers ::setStatus
     */
    function it_should_set_status()
    {
        $status = $this->faker->word;
        $mailProvider = $this->faker->word;
        /** @var Mail|Mockery\MockInterface $mailMock */
        $mailMock = Mockery::mock(Mail::class)->makePartial();

        $mailMock->shouldReceive('save')->once();

        $mailMock->setStatus($status, $mailProvider);

        $this->assertEquals($status, $mailMock->getAttribute('status'));
        $this->assertEquals($mailProvider, $mailMock->getAttribute('mail_provider'));
    }
}

export const Action = {
    FetchEmailList: 'fetchEmailList',
};

export const Mutation = {
    SetEmailList: 'setEmailList',
};

<?php


namespace App\Repositories\Mail;

use App\Models\Mail;
use Illuminate\Pagination\LengthAwarePaginator;

/**
 * Interface MailRepositoryInterface
 * @package App\Repositories\Mail
 */
interface MailRepositoryInterface
{
    /**
     * @param array $payload
     * @return mixed
     */
    public function create(array $payload);

    /**
     * @param string $uuid
     * @return Mail|null
     */
    public function findOneByUUID(string $uuid): ?Mail;

    /**
     * @param int $limit
     * @return LengthAwarePaginator
     */
    public function paginate(int $limit): LengthAwarePaginator;
}

<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Postman</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link href="{{ mix('css/app.css') }}" rel="stylesheet">
    <script>let config = {!! $config !!}</script>
</head>
<body>
<div id="app">
    <App/>
</div>
<script src="{{ asset('js/app.js') }}" defer></script>
</body>
</html>

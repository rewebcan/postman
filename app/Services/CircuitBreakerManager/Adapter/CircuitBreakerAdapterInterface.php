<?php

namespace App\Services\CircuitBreakerManager\Adapter;

/**
 * Interface CircuitBreakerAdapterInterface
 * @package App\Services\CircuitBreakerManager\Adapter
 */
interface CircuitBreakerAdapterInterface
{
    public function setSuccess(): void;

    /**
     * @param int $timeout
     */
    public function setOpen(int $timeout): void;

    /**
     * @param int $timeout
     * @param int $halfOpenTimeout
     */
    public function setHalfOpen(int $timeout, int $halfOpenTimeout): void;

    /**
     * @return bool
     */
    public function isOpen(): bool;

    /**
     * @param int $timeout
     * @return bool
     */
    public function incrementFailure(int $timeout): bool;

    /**
     * @return bool
     */
    public function isHalfOpen(): bool;

    /**
     * @param int $maxFailureCount
     * @return bool
     */
    public function reachRateLimit(int $maxFailureCount): bool;
}

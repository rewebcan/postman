<?php


namespace App\Services\CircuitBreakerManager\KeyHelper;

/**
 * Interface KeyHelperInterface
 * @package App\Services\CircuitBreakerManager\KeyHelper
 */
interface KeyHelperInterface
{
    /**
     * @return string
     */
    public function getOpenKey(): string;

    /**
     * @return string
     */
    public function getHalfOpenKey(): string;

    /**
     * @return string
     */
    public function getFailureKey(): string;
}

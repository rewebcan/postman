import Vue from 'vue';
import Vuex from 'vuex';
import {Action, Mutation} from "./types";
import {ApiEnums} from "../enums/api";

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        emailForm: {
            subject: '',
            recipients: [{name: '', email: ''}],
            replyTo: {name: '', email: ''},
            content: {type: '', value: ''},
        },
        emailList: {
            data: [],
            meta: {},
        },
    },
    mutations: {
        /**
         * @name setEmailList
         * @param state
         * @param emailList
         */
        [Mutation.SetEmailList]: (state, emailList) => {
            state.emailList = emailList
        },
    },
    actions: {
        async [Action.FetchEmailList]({commit}, page = 1, limit = 5) {
            const {data} = await window.axios.get(`${ApiEnums.EmailList}?page=${page}&per_page=${limit}`)
            commit(Mutation.SetEmailList, data)
        }
    }
})

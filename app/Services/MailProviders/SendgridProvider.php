<?php


namespace App\Services\MailProviders;

use App\Values\Mail\MailAddress;
use App\Values\Mail\MailPayload;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;
use Psr\Http\Message\ResponseInterface;
use Throwable;

/**
 * Class SendgridProvider
 * @package App\Services\MailProviders
 */
class SendgridProvider implements MailProviderInterface
{
    /** @var Client */
    private $httpClient;

    /**
     * SendgridProvider constructor.
     * @param Client $httpClient
     */
    public function __construct(Client $httpClient)
    {
        $this->httpClient = $httpClient;
    }

    /**
     * @param MailPayload $payload
     * @throws Throwable
     * @return ResponseInterface
     */
    public function send(MailPayload $payload): ResponseInterface
    {
        $method = 'POST';
        $url = config('services.sendgrid.endpoints.send_email');
        $apiToken = config('services.sendgrid.secret');
        $headers = [
            'Authorization' => "Bearer $apiToken",
            'Content-Type' => 'application/json',
        ];
        $recipients = array_map(function (MailAddress $mailAddress) {
            return $mailAddress->toArray();
        }, $payload->getRecipients());
        $requestBody = collect([
            'personalizations' => [
                ['to' => $recipients, 'subject' => $payload->getSubject()],
            ],
            'from' => $payload->getFrom()->toArray(),
            'reply_to' => $payload->getReplyTo()->toArray(),
            'content' => [
                ['type' => $payload->getContentType(), 'content' => $payload->getContent()],
            ],
            'categories' => [$payload->getUUID()],
        ]);

        return $this->httpClient->sendRequest(new Request($method, $url, $headers, $requestBody->toJson()));
    }
}

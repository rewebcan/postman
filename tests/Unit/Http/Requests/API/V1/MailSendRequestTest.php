<?php

namespace Tests\Unit\Http\Requests\API\V1;

use App\Http\Requests\API\V1\MailSendRequest;
use Tests\TestCase;

/**
 * Class MailSendRequestTest
 * @package Tests\Unit\Http\Requests\API\V1
 * @coversDefaultClass \App\Http\Requests\API\V1\MailSendRequest
 */
class MailSendRequestTest extends TestCase
{
    const TYPES = ['text', 'html', 'markdown'];

    /**
     * @test
     * @covers ::rules
     * @dataProvider rulesProvider
     * @param string $field
     * @param string|array $rule
     */
    function it_should_validate_rules(string $field, $rule)
    {
        $this->assertSame($rule, (new MailSendRequest())->rules()[$field]);
    }

    /**
     * @test
     * @covers ::rules
     */
    function it_should_count_validation_rules()
    {
        $this->assertCount(count($this->rulesProvider()), (new MailSendRequest())->rules());
    }

    /**
     * @return array
     */
    public function rulesProvider(): array
    {
        return [
            ['subject', 'required|string|max:255'],
            ['from.name', 'required|string'],
            ['from.email', 'required|email'],
            ['recipients', 'required|array'],
            ['recipients.*.name', 'required|string'],
            ['recipients.*.email', 'required|email'],
            ['replyTo.name', 'required|string'],
            ['replyTo.email', 'required|email'],
            ['content.type', 'required|in:' . implode(',', self::TYPES)],
            ['content.value', 'required|string'],
        ];
    }
}

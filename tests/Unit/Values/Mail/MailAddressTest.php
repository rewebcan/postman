<?php

namespace Tests\Unit\Values\Mail;

use App\Values\Mail\MailAddress;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

/**
 * Class MailAddressTest
 * @package Tests\Unit\Values\Mail
 * @coversDefaultClass \App\Values\Mail\MailAddress
 */
class MailAddressTest extends TestCase
{
    use WithFaker;
    /**
     * @test
     * @covers ::getFullName
     */
    function it_should_return_full_name()
    {
        $name = $this->faker->name;
        $this->assertEquals($name, (new MailAddress($name, ''))->getFullName());
    }

    /**
     * @test
     * @covers ::getFullName
     */
    function it_should_return_email()
    {
        $email = $this->faker->email;
        $this->assertEquals($email, (new MailAddress('', $email))->getEmailAddress());
    }
}

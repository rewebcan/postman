<?php

namespace Tests\Unit\Listeners;

use App\Events\MailSendingFailed;
use App\Listeners\LogMailSendingFailed;
use App\Models\Mail;
use App\Repositories\Mail\MailRepositoryInterface;
use App\Values\Mail\MailPayload;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

/**
 * Class LogMailSendingFailedTest
 * @package Tests\Unit\Listeners
 * @coversDefaultClass \App\Listeners\LogMailSendingFailed
 */
class LogMailSendingFailedTest extends TestCase
{
    use WithFaker;
    const FAILED = 'failed';

    /**
     * @test
     * @covers ::handle
     */
    function it_should_handle_mail_sending_event()
    {
        $mailPayload = new MailPayload([]);
        $provider = $this->faker->word;
        $mailModelMock = $this->createMock(Mail::class);
        $eventMock = $this->createMock(MailSendingFailed::class);
        $mailRepositoryMock = $this->createMock(MailRepositoryInterface::class);
        $listener = new LogMailSendingFailed($mailRepositoryMock);

        $mailRepositoryMock->expects($this->once())
            ->method('findOneByUUID')
            ->with($mailPayload->getUUID())
            ->willReturn($mailModelMock);
        $eventMock->expects($this->once())->method('getMailPayload')->willReturn($mailPayload);
        $eventMock->expects($this->once())->method('getMailProvider')->willReturn($provider);
        $mailModelMock->expects($this->once())->method('setStatus')->with(self::FAILED, $provider);

        $listener->handle($eventMock);
    }
}

<?php


namespace App\Services\CircuitBreakerManager;

use App\Services\CircuitBreakerManager\Adapter\CircuitBreakerAdapterInterface;

/**
 * Class ServiceStateManagerInterface
 * @package App\Services\StateChecker
 */
class CircuitBreakerManager
{
    /** @var CircuitBreakerAdapterInterface */
    private $cbAdapter;
    /** @var string */
    private $serviceName;

    /** @var CircuitBreakerSettings */
    protected $settings;

    /**
     * CircuitBreakerManager constructor.
     * @param CircuitBreakerAdapterInterface $adapter
     */
    public function __construct(CircuitBreakerAdapterInterface $adapter)
    {
        $this->cbAdapter = $adapter;
        $this->configureSettings(new CircuitBreakerSettings());
    }

    /**
     * @param CircuitBreakerSettings $settings
     * @return $this
     */
    public function configureSettings(CircuitBreakerSettings $settings): CircuitBreakerManager
    {
        $this->settings = $settings;

        return $this;
    }

    /**
     * @return bool
     */
    public function isAvailable(): bool
    {
        if ($this->cbAdapter->isOpen()) {
            return false;
        }

        if ($this->cbAdapter->reachRateLimit($this->settings->getMaxFailureCount())) {
            $this->cbAdapter->setOpen($this->settings->getTTL());
            $this->cbAdapter->setHalfOpen($this->settings->getTTL(), $this->settings->getHalfOpenTTL());

            return false;
        }

        return true;
    }

    /**
     * @return bool
     */
    public function failed(): bool
    {
        if ($this->cbAdapter->isHalfOpen()) {
            $this->cbAdapter->setOpen($this->settings->getTTL());
            $this->cbAdapter->setHalfOpen($this->settings->getTTL(), $this->settings->getHalfOpenTTL());

            return false;
        }

        return $this->cbAdapter->incrementFailure($this->settings->getTTL());
    }

    public function success()
    {
        $this->cbAdapter->setSuccess();
    }
}

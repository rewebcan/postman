<?php


namespace App\Services\MailProviders\Utilities;

use Illuminate\Support\Arr;

/**
 * Class MailProviderSwitcher
 * @package App\Services\MailProviders
 */
class MailProviderSwitcher
{
    const SENDGRID = 'sendgrid';
    const MAILJET = 'mailjet';

    const PROVIDERS = [self::SENDGRID, self::MAILJET];

    public function switch(string $currentProvider): string
    {
        return Arr::random(array_filter(self::PROVIDERS, function ($nextProvider) use ($currentProvider) {
            return $currentProvider !== $nextProvider;
        }));
    }
}

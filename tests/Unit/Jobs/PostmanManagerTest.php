<?php


namespace Tests\Unit\Jobs;

use App\Events\MailSending;
use App\Jobs\MailSender;
use App\Values\Mail\MailPayload;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Queue;
use Tests\TestCase;
use App\Jobs\PostmanManager;

/**
 * Class PostmanManagerTest
 * @package Tests\Unit\Jobs
 * @coversDefaultClass \App\Jobs\PostmanManager
 */
class PostmanManagerTest extends TestCase
{
    const SENDGRID = 'sendgrid';
    const MAILJET = 'mailjet';
    const TEXT = 'text';
    const HTML = 'html';

    use WithFaker;

    /**
     * @test
     * @covers ::__construct
     * @covers ::handle
     */
    function it_should_handle_postman_manager_job()
    {
        Event::fake();
        Queue::fake();

        $mailProvider = $this->faker->randomElement([self::SENDGRID, self::MAILJET]);
        $mailPayload = new MailPayload([
            'subject' => $this->faker->sentence,
            'from' => ['name' => $this->faker->name, 'email' => $this->faker->email],
            'recipients' => [['name' => $this->faker->name, 'email' => $this->faker->email]],
            'replyTo' => ['name' => $this->faker->name, 'email' => $this->faker->email],
            'content' => [
                'type' => $this->faker->randomElement([self::TEXT, self::HTML]),
                'content' => $this->faker->sentence,
            ],
        ]);

        $postmanManager = new PostmanManager($mailProvider, $mailPayload);

        $postmanManager->handle();

        Event::assertDispatched(MailSending::class, function (MailSending $event) use ($mailPayload) {
            $this->assertProperty($event, 'emailPayload', $mailPayload);

            return true;
        });

        Queue::assertPushed(MailSender::class, function (MailSender $mailSender) use ($mailProvider, $mailPayload) {
            $this->assertProperty($mailSender, 'mailProvider', $mailProvider);
            $this->assertProperty($mailSender, 'mailPayload', $mailPayload);

            return true;
        });
    }
}

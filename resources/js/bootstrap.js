/**
 * Bootstrap
 */
window.axios = require('axios');
window.axios.defaults.baseURL = config.api;
window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
window.axios.interceptors.request.use(
    function (config) {
        console.log("Axios request config", config);
        return new Promise(resolve => setTimeout(() => resolve(config), 400));
    },
    function (error) {
        console.log("Axios request error", error);
        return Promise.reject(error);
    }
);

window.axios.interceptors.response.use(
    function (response) {
        console.log("Axios response", response);
        return response;
    },
    function (error) {
        console.log("Axios response error", error);
        return Promise.reject(error);
    }
);

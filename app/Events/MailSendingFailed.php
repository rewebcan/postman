<?php

namespace App\Events;

use App\Values\Mail\MailPayload;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Foundation\Events\Dispatchable;

class MailSendingFailed
{
    use Dispatchable, InteractsWithSockets;

    /** @var string */
    private $mailProvider;
    /** @var array */
    private $mailPayload;

    /**
     * @param string $mailProvider
     * @param MailPayload $mailPayload
     */
    public function __construct(string $mailProvider, MailPayload $mailPayload)
    {
        $this->mailProvider = $mailProvider;
        $this->mailPayload = $mailPayload;
    }

    /**
     * @return string
     */
    public function getMailProvider(): string
    {
        return $this->mailProvider;
    }

    /**
     * @return MailPayload
     */
    public function getMailPayload(): MailPayload
    {
        return $this->mailPayload;
    }
}

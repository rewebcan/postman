<?php


namespace App\Repositories\Mail;

use App\Models\Mail;
use Illuminate\Pagination\LengthAwarePaginator;

/**
 * Class MailRepository
 * @package App\Repositories\Mail
 */
class MailRepository implements MailRepositoryInterface
{
    /** @var Mail */
    private $mail;

    /**
     * MailRepository constructor.
     * @param Mail $mail
     */
    public function __construct(Mail $mail)
    {
        $this->mail = $mail;
    }

    /**
     * @param array $payload
     * @return mixed|void
     */
    public function create(array $payload): void
    {
        $this->mail->create($payload);
    }


    /**
     * @param string $uuid
     * @return Mail|null
     */
    public function findOneByUUID(string $uuid): ?Mail
    {
        return $this->mail->where('uuid', $uuid)->first();
    }

    /**
     * @param int $limit
     * @return LengthAwarePaginator
     */
    public function paginate(int $limit): LengthAwarePaginator
    {
        return $this->mail->paginate($limit);
    }
}

require('./bootstrap');

import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from './store'
import VueFormulate from '@braid/vue-formulate'
import Toasted from 'vue-toasted';

Vue.axios = window.axios;

Vue.use(VueFormulate, {
    classes: {
        outer: 'form-group',
        input: 'form-control',
        inputHasErrors: 'is-invalid',
        help: 'form-text text-muted',
        errors: 'list-unstyled text-danger',
    }
})
Vue.use(Toasted)

new Vue({el: '#app', router, store, render: h => h(App)});

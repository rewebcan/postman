<?php

namespace App\Events;

use App\Values\Mail\MailPayload;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class MailSent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /** @var string */
    private $mailProvider;
    /** @var array */
    private $mailPayload;

    /**
     * Create a new event instance.
     *
     * @param string $mailProvider
     * @param MailPayload $mailPayload
     */
    public function __construct(string $mailProvider, MailPayload $mailPayload)
    {
        $this->mailProvider = $mailProvider;
        $this->mailPayload = $mailPayload;
    }

    /**
     * @return string
     */
    public function getMailProvider(): string
    {
        return $this->mailProvider;
    }

    /**
     * @return MailPayload
     */
    public function getMailPayload(): MailPayload
    {
        return $this->mailPayload;
    }
}

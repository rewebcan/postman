<?php


namespace App\Services\MailProviders;

use App\Values\Mail\MailPayload;
use Psr\Http\Message\ResponseInterface;

/**
 * Interface MailProviderInterface
 * @package App\Services\MailProviders
 */
interface MailProviderInterface
{
    /**
     * @param MailPayload $payload
     * @return ResponseInterface
     */
    public function send(MailPayload $payload): ResponseInterface;
}

<?php

namespace App\Jobs;

use App\Events\MailSending;
use App\Values\Mail\MailPayload;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Queue;

/**
 * Class PostmanManager
 * @package App\Jobs
 */
class PostmanManager implements ShouldQueue
{
    use Queueable, InteractsWithQueue, Queueable;

    /** @var string */
    private $mailProvider;
    /** @var array */
    private $mailPayload;

    /**
     * PostManager constructor.
     * @param string $mailProvider
     * @param MailPayload $mailPayload
     */
    public function __construct(string $mailProvider, MailPayload $mailPayload)
    {
        $this->mailProvider = $mailProvider;
        $this->mailPayload = $mailPayload;
    }

    /**
     * @return void
     */
    public function handle(): void
    {
        event(new MailSending($this->mailPayload));

        Queue::push(new MailSender($this->mailProvider, $this->mailPayload));
    }
}

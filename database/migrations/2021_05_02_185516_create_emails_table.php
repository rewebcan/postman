<?php

use App\Enums\MailStatusEnums;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mails', function (Blueprint $table) {
            $table->id();
            $table->uuid('uuid');
            $table->string('subject');
            $table->string('type');
            $table->text('content');
            $table->string('mail_provider')->nullable();
            $table->enum('status', [
                MailStatusEnums::SENDING,
                MailStatusEnums::SUCCESS,
                MailStatusEnums::FAILED,
            ])->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('emails');
    }
}

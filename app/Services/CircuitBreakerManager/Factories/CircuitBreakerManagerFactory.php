<?php


namespace App\Services\CircuitBreakerManager\Factories;

use App\Services\CircuitBreakerManager\Adapter\RedisCircuitBreakerAdapter;
use App\Services\CircuitBreakerManager\CircuitBreakerManager;
use App\Services\CircuitBreakerManager\KeyHelper\KeyHelper;

/**
 * Class CircuitBreakerManagerFactory
 * @package App\Services\CircuitBreakerManager\Factories
 */
class CircuitBreakerManagerFactory
{
    /**
     * @param string $provider
     * @return CircuitBreakerManager
     */
    public function make(string $provider): CircuitBreakerManager
    {
        $keyHelper = new KeyHelper($provider);
        $cbAdapter = new RedisCircuitBreakerAdapter($keyHelper);

        return new CircuitBreakerManager($cbAdapter);
    }
}

<?php

namespace App\Values\Mail;

use Illuminate\Support\Arr;
use Ramsey\Uuid\Uuid;

/**
 * Class MailPayload
 * @package App\Values\DataObjects
 */
final class MailPayload
{
    /** @var string */
    private $uuid;

    /** @var array */
    private $payload;

    /**
     * MailPayload constructor.
     * @param array $payload
     */
    public function __construct(array $payload)
    {
        $this->uuid = Uuid::uuid4();

        $this->payload = $payload;
    }

    /**
     * @return string
     */
    public function getUUID(): string
    {
        return $this->uuid;
    }

    /**
     * @return string
     */
    public function getSubject(): string
    {
        return Arr::get($this->payload, 'subject');
    }

    /**
     * @return MailAddress
     */
    public function getFrom(): MailAddress
    {
        return new MailAddress(Arr::get($this->payload, 'from.name'), Arr::get($this->payload, 'from.email'));
    }

    /**
     * @return MailAddress[]|array
     */
    public function getRecipients(): array
    {
        return array_map(function (array $recipient) {
            return new MailAddress($recipient['name'], $recipient['email']);
        }, Arr::get($this->payload, 'recipients'));
    }

    /**
     * @return MailAddress
     */
    public function getReplyTo(): MailAddress
    {
        return new MailAddress(Arr::get($this->payload, 'replyTo.name'), Arr::get($this->payload, 'replyTo.email'));
    }

    /**
     * @return string
     */
    public function getContent(): string
    {
        return Arr::get($this->payload, 'content.value');
    }

    /**
     * @return string
     */
    public function getContentType(): string
    {
        return Arr::get($this->payload, 'content.type');
    }
}

<?php

namespace App\Http\Controllers\App;

use App\Http\Controllers\Controller;
use Illuminate\View\View;

class AppController extends Controller
{
    /**
     * @return View
     */
    public function index(): View
    {
        $url = config('app.url');
        $config = ['base' => $url, 'api' => sprintf('%s/api', $url)];

        return view('app')->with('config', json_encode($config));
    }
}

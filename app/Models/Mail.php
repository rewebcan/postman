<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Concerns\HasTimestamps;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Email
 * @package App\Models
 * @property mixed|string status
 * @property mixed|string mail_provider
 * @method create(array $payload)
 * @method where(string $string, string $uuid)
 * @method paginate(int $limit)
 */
class Mail extends Model
{
    use HasFactory, HasTimestamps, SoftDeletes;

    protected $guarded = ['id'];

    /**
     * @param string $status
     * @param string $mailProvider
     * @return void
     */
    public function setStatus(string $status, string $mailProvider): void
    {
        $this->status = $status;
        $this->mail_provider = $mailProvider;

        $this->save();
    }
}

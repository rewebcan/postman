<?php

namespace App\Http\Requests\API\V1;

use App\Http\Requests\API\APIRequest;

/**
 * Class MailListRequest
 * @package App\Http\Requests\API\V1
 */
class MailListRequest extends APIRequest
{
    /**
     * @return string[]
     */
    public function rules(): array
    {
        return [
            'per_page' => 'int|max:50',
            'page' => 'int',
        ];
    }
}

<?php


namespace App\Enums;

/**
 * Class MailStatusEnums
 * @package App\Enums
 */
final class MailStatusEnums
{
    const SENDING = 'sending';
    const SUCCESS = 'success';
    const FAILED = 'failed';
}

<?php


namespace App\Enums;

/**
 * Class MailTypeEnums
 * @package App\Enums
 */
final class MailTypeEnums
{
    const TEXT = 'text';
    const HTML = 'html';
    const MARKDOWN = 'markdown';
}

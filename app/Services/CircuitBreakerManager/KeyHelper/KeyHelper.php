<?php


namespace App\Services\CircuitBreakerManager\KeyHelper;

/**
 * Class KeyHelper
 * @package App\Services\CircuitBreakerManager\KeyHelper
 */
class KeyHelper implements KeyHelperInterface
{
    /** @var string */
    public const OPEN = 'postman::%s::open';
    /** @var string */
    public const HALF_OPEN = 'postman::%s::half_open';
    /** @var string */
    public const FAILURE = 'postman::%s::failure';

    /** @var string */
    private $redisNamespace;

    /**
     * KeyHelper constructor.
     * @param string $namespace
     */
    public function __construct(string $namespace)
    {
        $this->redisNamespace = $namespace;
    }

    /**
     * @return string
     */
    public function getOpenKey(): string
    {
        return sprintf(self::OPEN, $this->redisNamespace);
    }


    /**
     * @return string
     */
    public function getHalfOpenKey(): string
    {
        return sprintf(self::HALF_OPEN, $this->redisNamespace);
    }


    /**
     * @return string
     */
    public function getFailureKey(): string
    {
        return sprintf(self::FAILURE, $this->redisNamespace);
    }
}

<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use App\Http\Requests\API\V1\MailListRequest;
use App\Http\Requests\API\V1\MailSendRequest;
use App\Http\Resources\MailResource;
use App\Jobs\PostmanManager;
use App\Repositories\Mail\MailRepositoryInterface;
use App\Values\Mail\MailPayload;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Support\Facades\Queue;

/**
 * Class PostmanController
 * @package App\Http\Controllers\API\V1
 */
class PostmanController extends Controller
{
    const DEFAULT_PAGE_SIZE = 10;

    /**
     * @param MailListRequest $request
     * @param MailRepositoryInterface $mailRepository
     * @return AnonymousResourceCollection
     */
    public function index(MailListRequest $request, MailRepositoryInterface $mailRepository): AnonymousResourceCollection
    {
        return MailResource::collection(
            $mailRepository->paginate($request->query('per_page', self::DEFAULT_PAGE_SIZE))
        );
    }

    /**
     * @param MailSendRequest $request
     * @return string
     */
    public function store(MailSendRequest $request): string
    {
        Queue::push(new PostmanManager(config('services.primary_email_provider'), new MailPayload($request->validated())));

        return 'ok';
    }
}

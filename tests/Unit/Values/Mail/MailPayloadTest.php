<?php

namespace Tests\Unit\Values\Mail;

use App\Values\Mail\MailAddress;
use App\Values\Mail\MailPayload;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Str;
use Tests\TestCase;

/**
 * Class MailPayloadTest
 * @package Tests\Unit\Values\Mail
 * @coversDefaultClass \App\Values\Mail\MailPayload
 */
class MailPayloadTest extends TestCase
{
    use WithFaker;

    /**
     * @test
     * @covers ::__construct
     * @covers ::getFrom
     */
    function it_should_return_from_recipient()
    {
        $name = $this->faker->name;
        $email = $this->faker->email;
        $parameters = ['from' => compact('name', 'email')];
        $campaignPayload = new MailPayload($parameters);

        $this->assertEquals(new MailAddress($name, $email), $campaignPayload->getFrom());
    }

    /**
     * @test
     * @covers ::getRecipients
     */
    function it_should_return_to_recipient()
    {
        $firstRecipient = ['name' => $this->faker->name, 'email' => $this->faker->email];
        $secondRecipient = ['name' => $this->faker->name, 'email' => $this->faker->email];
        $parameters = ['recipients' => [$firstRecipient, $secondRecipient]];
        $campaignPayload = new MailPayload($parameters);

        $this->assertEquals(
            [
                new MailAddress($firstRecipient['name'], $firstRecipient['email']),
                new MailAddress($secondRecipient['name'], $secondRecipient['email']),
            ],
            $campaignPayload->getRecipients()
        );
    }

    /**
     * @test
     * @covers ::getReplyTo
     */
    function it_should_return_reply_to_recipient()
    {
        $name = $this->faker->name;
        $email = $this->faker->email;
        $parameters = ['replyTo' => compact('name', 'email')];
        $campaignPayload = new MailPayload($parameters);

        $this->assertEquals(new MailAddress($name, $email), $campaignPayload->getReplyTo());
    }

    /**
     * @test
     * @covers ::getContent
     */
    function it_should_return_content()
    {
        $value = $this->faker->text;
        $parameters = ['content' => compact('value')];
        $campaignPayload = new MailPayload($parameters);

        $this->assertEquals($value, $campaignPayload->getContent());
    }

    /**
     * @test
     * @covers ::getContentType
     */
    function it_should_return_content_type()
    {
        $type = $this->faker->text;
        $parameters = ['content' => compact('type')];
        $campaignPayload = new MailPayload($parameters);

        $this->assertEquals($type, $campaignPayload->getContentType());
    }

    /**
     * @test
     * @covers ::getSubject
     */
    function it_should_return_subject()
    {
        $subject = $this->faker->text(10);
        $parameters = compact('subject');
        $campaignPayload = new MailPayload($parameters);

        $this->assertEquals($subject, $campaignPayload->getSubject());
    }

    /**
     * @test
     * @covers ::getUUID
     * @covers ::__construct
     */
    function it_should_return_campaign_id()
    {
        $uuidLengthCount = 36;
        $campaignPayload = new MailPayload([]);

        $this->assertEquals($uuidLengthCount, Str::length($campaignPayload->getUUID()));
    }
}

<?php

namespace App\Listeners;

use App\Enums\MailStatusEnums;
use App\Events\MailSendingFailed;
use App\Repositories\Mail\MailRepositoryInterface;

/**
 * Class LogMailSendingFailed
 * @package App\Listeners
 */
class LogMailSendingFailed
{
    /**
     * @var MailRepositoryInterface
     */
    private $mailRepository;

    /**
     * LogMailSendingFailed constructor.
     * @param MailRepositoryInterface $mailRepository
     */
    public function __construct(MailRepositoryInterface $mailRepository)
    {
        $this->mailRepository = $mailRepository;
    }

    /**
     * @param MailSendingFailed $event
     * @return void
     */
    public function handle(MailSendingFailed $event)
    {
        $mailLog = $this->mailRepository->findOneByUUID($event->getMailPayload()->getUUID());

        $mailLog->setStatus(MailStatusEnums::FAILED, $event->getMailProvider());
    }
}

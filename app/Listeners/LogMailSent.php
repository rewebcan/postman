<?php

namespace App\Listeners;

use App\Enums\MailStatusEnums;
use App\Events\MailSent;
use App\Repositories\Mail\MailRepositoryInterface;

/**
 * Class LogMailSent
 * @package App\Listeners
 */
class LogMailSent
{
    /**
     * @var MailRepositoryInterface
     */
    private $mailRepository;

    /**
     * LogMailSent constructor.
     * @param MailRepositoryInterface $mailRepository
     */
    public function __construct(MailRepositoryInterface $mailRepository)
    {
        $this->mailRepository = $mailRepository;
    }

    /**
     * @param MailSent $event
     * @return void
     */
    public function handle(MailSent $event)
    {
        $mailLog = $this->mailRepository->findOneByUUID($event->getMailPayload()->getUUID());

        $mailLog->setStatus(MailStatusEnums::SUCCESS, $event->getMailProvider());
    }
}

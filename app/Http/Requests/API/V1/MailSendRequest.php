<?php


namespace App\Http\Requests\API\V1;

use App\Enums\MailTypeEnums;
use App\Http\Requests\API\APIRequest;

/**
 * Class MailSendRequest
 * @package App\Http\Requests\API\V1
 */
class MailSendRequest extends APIRequest
{
    /**
     * @return array
     */
    public function rules(): array
    {
        $types = implode(',', [MailTypeEnums::TEXT, MailTypeEnums::HTML, MailTypeEnums::MARKDOWN]);

        return [
            'subject' => 'required|string|max:255',
            'from.name' => 'required|string',
            'from.email' => 'required|email',
            'recipients' => 'required|array',
            'recipients.*.name' => 'required|string',
            'recipients.*.email' => 'required|email',
            'replyTo.name' => 'required|string',
            'replyTo.email' => 'required|email',
            'content.type' => 'required|in:' . $types,
            'content.value' => 'required|string',
        ];
    }
}

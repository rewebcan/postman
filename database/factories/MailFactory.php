<?php

namespace Database\Factories;

use App\Enums\MailStatusEnums;
use App\Models\Mail;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Foundation\Testing\WithFaker;

class MailFactory extends Factory
{
    use WithFaker;

    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Mail::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition(): array
    {
        return [
            'uuid' => $this->faker->uuid,
            'subject' => $this->faker->sentence,
            'type' => $this->faker->randomElement(['TEXT', 'HTML']),
            'content' => $this->faker->text,
            'mail_provider' => $this->faker->word,
            'status' => $this->faker->randomElement([
                MailStatusEnums::SENDING,
                MailStatusEnums::SUCCESS,
                MailStatusEnums::FAILED,
            ]),
        ];
    }
}

<?php


namespace Tests\Unit\Services\CircuitBreakerManager\Adapter;

use App\Services\CircuitBreakerManager\Adapter\RedisCircuitBreakerAdapter;
use App\Services\CircuitBreakerManager\KeyHelper\KeyHelper;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Redis;
use Tests\TestCase;

/**
 * Class RedisCircuitBreakerAdapterTest
 * @package Tests\Unit\Services\CircuitBreakerManager
 * @coversDefaultClass \App\Services\CircuitBreakerManager\Adapter\RedisCircuitBreakerAdapter
 */
class RedisCircuitBreakerAdapterTest extends TestCase
{
    use WithFaker;

    /** @var KeyHelper */
    private $keyHelper;
    /** @var RedisCircuitBreakerAdapter */
    private $redisCircuitBreakerAdapter;

    /**
     * @return void
     */
    function setUp(): void
    {
        parent::setUp();

        $namespace = $this->faker->word;
        $this->keyHelper = new KeyHelper($namespace);
        $this->redisCircuitBreakerAdapter = new RedisCircuitBreakerAdapter($this->keyHelper);
    }

    /**
     * @test
     * @covers ::__construct
     * @covers ::setSuccess
     */
    function it_should_set_success()
    {
        Redis::shouldReceive('transaction')->once();

        $this->redisCircuitBreakerAdapter->setSuccess();
    }

    /**
     * @test
     * @covers ::setOpen
     */
    function it_should_set_open()
    {
        $this->travelTo(now());
        $timeout = $this->faker->randomDigit;

        Redis::shouldReceive('set')->once()->with($this->keyHelper->getOpenKey(), time(), $timeout);

        $this->redisCircuitBreakerAdapter->setOpen($timeout);
    }

    /**
     * @test
     * @covers ::setHalfOpen
     */
    function it_should_set_half_open()
    {
        $this->travelTo(now());
        $timeout = $this->faker->randomDigit;
        $halfOpenTimeout = $this->faker->randomDigit;

        Redis::shouldReceive('set')
            ->once()
            ->with($this->keyHelper->getHalfOpenKey(), time(), $timeout + $halfOpenTimeout);

        $this->redisCircuitBreakerAdapter->setHalfOpen($timeout, $halfOpenTimeout);
    }

    /**
     * @test
     * @covers ::incrementFailure
     */
    function it_should_increment_failure()
    {
        $timeout = $this->faker->randomDigit;
        Redis::shouldReceive('get')->once()->with($this->keyHelper->getFailureKey())->andReturn(true);
        Redis::shouldReceive('incr')->once()->with($this->keyHelper->getFailureKey());

        $this->redisCircuitBreakerAdapter->incrementFailure($timeout);
    }

    /**
     * @test
     * @covers ::isOpen
     */
    function it_should_return_open_status()
    {
        Redis::shouldReceive('exists')->once()->with($this->keyHelper->getOpenKey())->andReturn(true);

        $this->redisCircuitBreakerAdapter->isOpen();
    }

    /**
     * @test
     * @covers ::reachRateLimit
     */
    function it_should_return_false_when_does_not_reach_limit()
    {
        $maxFailureCount = $this->faker->randomDigit;

        Redis::shouldReceive('get')->once()->with($this->keyHelper->getFailureKey())->andReturn($maxFailureCount - 1);

        $this->assertFalse($this->redisCircuitBreakerAdapter->reachRateLimit($maxFailureCount));
    }

    /**
     * @test
     * @covers ::reachRateLimit
     */
    function it_should_return_true_when_reaches_limit()
    {
        $maxFailureCount = $this->faker->randomDigit;

        Redis::shouldReceive('get')->once()->with($this->keyHelper->getFailureKey())->andReturn($maxFailureCount + 1);

        $this->assertTrue($this->redisCircuitBreakerAdapter->reachRateLimit($maxFailureCount));
    }
}

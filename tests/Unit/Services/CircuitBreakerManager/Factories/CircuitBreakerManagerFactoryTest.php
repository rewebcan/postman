<?php


namespace Tests\Unit\Services\CircuitBreakerManager\Factories;

use App\Services\CircuitBreakerManager\CircuitBreakerManager;
use App\Services\CircuitBreakerManager\Factories\CircuitBreakerManagerFactory;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

/**
 * Class CircuitBreakerManagerFactoryTest
 * @package Tests\Unit\Services\CircuitBreakerManager\Factories
 * @coversDefaultClass \App\Services\CircuitBreakerManager\Factories\CircuitBreakerManagerFactory
 */
class CircuitBreakerManagerFactoryTest extends TestCase
{
    use WithFaker;

    /**
     * @test
     * @covers ::make
     */
    function it_should_make_and_return_circuit_breaker_manager()
    {
        $provider = $this->faker->name;

        $this->assertInstanceOf(
            CircuitBreakerManager::class,
            (new CircuitBreakerManagerFactory())->make($provider)
        );
    }
}

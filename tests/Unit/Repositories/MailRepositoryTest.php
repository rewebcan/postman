<?php


namespace Tests\Unit\Repositories;

use App\Models\Mail;
use App\Repositories\Mail\MailRepository;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Pagination\LengthAwarePaginator;
use Tests\TestCase;

/**
 * Class MailRepositoryTest
 * @package Tests\Unit\Repositories
 * @coversDefaultClass \App\Repositories\Mail\MailRepository
 */
class MailRepositoryTest extends TestCase
{
    use WithFaker;

    /** @var Mail */
    private $mailModelMock;
    /** @var MailRepository */
    private $mailRepository;

    protected function setUp(): void
    {
        parent::setUp();

        $this->mailModelMock = $this->getMockBuilder(Mail::class)
            ->addMethods(['create', 'where', 'paginate'])
            ->getMock();
        $this->mailRepository = new MailRepository($this->mailModelMock);
    }

    /**
     * @test
     * @covers ::create
     */
    function it_should_create_mail()
    {
        $payload = ['subject' => $this->faker->sentence];

        $this->mailModelMock->expects($this->once())->method('create')->with($payload);

        $this->mailRepository->create($payload);
    }

    /**
     * @test
     * @covers ::findOneByUUID
     */
    function it_should_return_mail()
    {
        $mailModel = new Mail();
        $uuid = $this->faker->uuid;

        $this->mailModelMock->expects($this->once())
            ->method('where')
            ->with('uuid', $uuid)
            ->willReturn(collect([$mailModel]));

        $this->assertEquals($mailModel, $this->mailRepository->findOneByUUID($uuid));
    }

    /**
     * @test
     * @covers ::findOneByUUID
     */
    function it_should_return_null_when_does_not_exists()
    {
        $uuid = $this->faker->uuid;

        $this->mailModelMock->expects($this->once())
            ->method('where')
            ->with('uuid', $uuid)
            ->willReturn(collect([null]));

        $this->assertNull($this->mailRepository->findOneByUUID($uuid));
    }

    /**
     * @test
     * @covers ::__construct
     * @covers ::paginate
     */
    function it_should_return_paginated_results()
    {
        $size = $this->faker->randomDigit;
        $lengthAwarePaginator = $this->createMock(LengthAwarePaginator::class);

        $this->mailModelMock->expects($this->once())
            ->method('paginate')
            ->with($size)
            ->willReturn($lengthAwarePaginator);

        $this->assertEquals($lengthAwarePaginator, $this->mailRepository->paginate($size));
    }
}

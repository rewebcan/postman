<?php

namespace Tests\Unit\Commands;

use App\Console\Commands\PostmanSendMail;
use App\Jobs\PostmanManager;
use App\Values\Mail\MailPayload;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Queue;
use Tests\TestCase;

/**
 * Class PostmanSendMailTest
 * @package Tests\Unit\Commands
 * @coversDefaultClass \App\Console\Commands\PostmanSendMail
 */
class PostmanSendMailTest extends TestCase
{
    use WithFaker;

    /**
     * @test
     * @covers ::getMailPayload
     */
    function it_should_return_mail_payload()
    {
        [$fromName, $fromEmail, $replyToName, $replyToEmail, $subject, $recipient, $content] = [
            $this->faker->name,
            $this->faker->email,
            $this->faker->name,
            $this->faker->email,
            $this->faker->sentence,
            [implode(":", [$this->faker->name, $this->faker->email])],
            $this->faker->text,
        ];
        $commandMock = $this->getMockBuilder(PostmanSendMail::class)
            ->onlyMethods(['option'])
            ->getMock();
        $commandMock->expects($this->atLeastOnce())
            ->method('option')
            ->withConsecutive(
                ['fromName'],
                ['fromEmail'],
                ['replyToName'],
                ['replyToEmail'],
                ['subject'],
                ['recipient'],
                ['content'],
            )
            ->willReturnOnConsecutiveCalls(
                $fromName,
                $fromEmail,
                $replyToName,
                $replyToEmail,
                $subject,
                $recipient,
                $content,
            );

        $this->assertInstanceOf(
            MailPayload::class,
            $this->invokeMethod($commandMock, 'getMailPayload'),
        );
    }

    /**
     * @test
     * @covers ::handle
     */
    function it_should_handle_sending_mail()
    {
        Queue::fake();

        $payload = new MailPayload([
            'subject' => $this->faker->sentence,
            'from' => ['name' => $this->faker->name, 'email' => $this->faker->email],
            'recipients' => [['name' => $this->faker->name, 'email' => $this->faker->email]],
            'replyTo' => ['name' => $this->faker->name, 'email' => $this->faker->email],
            'content' => ['type' => 'text', 'value' => $this->faker->email],
        ]);

        $commandMock = $this->getMockBuilder(PostmanSendMail::class)
            ->onlyMethods(['getMailPayload', 'info'])
            ->getMock();

        $commandMock->expects($this->once())->method('getMailPayload')->willReturn($payload);
        $commandMock->expects($this->once())->method('info')->willReturn('OK');

        $commandMock->handle();

        Queue::assertPushed(PostmanManager::class, function (PostmanManager $postmanManager) use ($payload) {
            $this->assertProperty($postmanManager, 'mailProvider', config('services.primary_email_provider'));
            $this->assertProperty($postmanManager, 'mailPayload', $payload);

            return true;
        });
    }
}

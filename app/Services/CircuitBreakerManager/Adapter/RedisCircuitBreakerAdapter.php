<?php


namespace App\Services\CircuitBreakerManager\Adapter;

use App\Services\CircuitBreakerManager\KeyHelper\KeyHelperInterface;
use Illuminate\Support\Facades\Redis;

/**
 * Class RedisCircuitBreakerAdapter
 * @package App\Services\CircuitBreakerManager\Adapter
 */
class RedisCircuitBreakerAdapter implements CircuitBreakerAdapterInterface
{
    /** @var KeyHelperInterface */
    private $keyHelper;

    /**
     * RedisCircuitBreakerAdapter constructor.
     * @param KeyHelperInterface $keyHelper
     */
    public function __construct(KeyHelperInterface $keyHelper)
    {
        $this->keyHelper = $keyHelper;
    }

    public function setSuccess(): void
    {
        Redis::transaction(function ($redis) {
            $redis->del($this->keyHelper->getOpenKey());
            $redis->del($this->keyHelper->getHalfOpenKey());
            $redis->del($this->keyHelper->getFailureKey());
        });
    }

    /**
     * @param int $timeout
     */
    public function setOpen(int $timeout): void
    {
        Redis::set($this->keyHelper->getOpenKey(), time(), $timeout);
    }

    /**
     * @param int $timeout
     * @param int $halfOpenTimeout
     */
    public function setHalfOpen(int $timeout, int $halfOpenTimeout): void
    {
        Redis::set(
            $this->keyHelper->getHalfOpenKey(),
            time(),
            $timeout + $halfOpenTimeout
        );
    }

    /**
     * @param int $timeout
     * @return bool
     */
    public function incrementFailure(int $timeout): bool
    {
        $failureKey = $this->keyHelper->getFailureKey();
        $doesFailureKeyExists = (bool)Redis::get($failureKey);

        if (!$doesFailureKeyExists) {
            Redis::transaction(function ($redis) use ($failureKey, $timeout) {
                $result = $redis->incr($failureKey);
                $redis->expire($failureKey, $timeout);

                return (bool)$result ?? false;
            });
        }

        return (bool)Redis::incr($failureKey);
    }

    /**
     * @return bool
     */
    public function isOpen(): bool
    {
        return Redis::exists($this->keyHelper->getOpenKey());
    }

    /**
     * @return bool
     */
    public function isHalfOpen(): bool
    {
        return Redis::exists($this->keyHelper->getHalfOpenKey());
    }

    /**
     * @param int $maxFailureCount
     * @return bool
     */
    public function reachRateLimit(int $maxFailureCount): bool
    {
        $failCount = (int)Redis::get($this->keyHelper->getFailureKey());

        return $failCount >= $maxFailureCount;
    }
}

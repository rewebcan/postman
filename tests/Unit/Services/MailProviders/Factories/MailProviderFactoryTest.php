<?php

namespace Tests\Unit\Services\MailProviders\Factories;

use App\Services\MailProviders\Factories\MailProviderFactory;
use App\Services\MailProviders\MailjetProvider;
use App\Services\MailProviders\SendgridProvider;
use GuzzleHttp\Client;
use Tests\TestCase;

/**
 * Class MailProviderFactoryTest
 * @package Tests\Unit\Services\MailProviders\Factories
 * @coversDefaultClass \App\Services\MailProviders\Factories\MailProviderFactory
 */
class MailProviderFactoryTest extends TestCase
{
    const SENDGRID = 'sendgrid';
    const MAILJET = 'mailjet';

    /**
     * @test
     * @covers ::__construct
     * @covers ::make
     */
    function it_should_return_sendgrid_provider()
    {
        $clientMock = $this->createMock(Client::class);

        $this->assertInstanceOf(
            SendgridProvider::class,
            (new MailProviderFactory($clientMock))->make(self::SENDGRID)
        );
    }

    /**
     * @test
     * @covers ::__construct
     * @covers ::make
     */
    function it_should_return_mailjet_provider()
    {
        $clientMock = $this->createMock(Client::class);

        $this->assertInstanceOf(
            MailjetProvider::class,
            (new MailProviderFactory($clientMock))->make(self::MAILJET)
        );
    }
}

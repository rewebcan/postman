import EmailArchive from "../pages/EmailListing";
import EmailSend from "../pages/EmailSend";
import VueRouter from "vue-router";
import Vue from "vue";

Vue.use(VueRouter)

const routes = [
    {path: '/', name: 'email-archive', component: EmailArchive},
    {path: '/email-send', name: 'email-send', component: EmailSend}
];

export default new VueRouter({routes});

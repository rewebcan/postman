<?php


namespace App\Enums;

final class MailProviderEnums
{
    const SENDGRID = 'sendgrid';
    const MAILJET = 'mailjet';
}

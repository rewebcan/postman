<?php

namespace App\Console\Commands;

use App\Jobs\PostmanManager;
use App\Values\Mail\MailPayload;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Queue;

class PostmanSendMail extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'postman:send-mail
                            {--fromName= : From Name}
                            {--fromEmail= : From Email}
                            {--replyToName= : Reply to Name}
                            {--replyToEmail= : Reply To Email}
                            {--subject= : Subject}
                            {--recipient=* : Allows Multiple Recipients in "John Doe:john.doe@gmail.com" format}
                            {--content= : Mail Content}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'It sends an email with given payload';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $payload = $this->getMailPayload();

        if (is_null($payload)) {
            $this->error('Invalid arguments, please see --help');

            return;
        }

        Queue::push(new PostmanManager(config('services.primary_email_provider'), $payload));

        $this->info('OK');
    }

    /**
     * @return MailPayload|null
     */
    protected function getMailPayload(): ?MailPayload
    {
        [$fromName, $fromEmail, $replyToName, $replyToMail, $subject, $recipients, $content] = [
            $this->option('fromName'),
            $this->option('fromEmail'),
            $this->option('replyToName'),
            $this->option('replyToEmail'),
            $this->option('subject'),
            $this->option('recipient'),
            $this->option('content'),
        ];

        $clearedRecipients = array_map(function ($recipientItem) {
            $split = explode(":", $recipientItem);

            return ['name' => $split[0], 'email' => $split[1]];
        }, $recipients);

        return new MailPayload([
            'subject' => $subject,
            'from' => [
                'name' => $fromName,
                'email' => $fromEmail,
            ],
            'replyTo' => [
                'name' => $replyToName,
                'email' => $replyToMail,
            ],
            'recipients' => $clearedRecipients,
            'content' => [
                'type' => 'TEXT',
                'value' => $content,
            ],
        ]);
    }
}

<?php

namespace Tests\Unit\Services\MailProviders\Utilities;

use App\Services\MailProviders\Utilities\MailProviderSwitcher;
use PHPUnit\Framework\TestCase;

/**
 * Class MailProviderSwitcherTest
 * @package Tests\Services\MailProviders
 * @coversDefaultClass \App\Services\MailProviders\Utilities\MailProviderSwitcher
 */
class MailProviderSwitcherTest extends TestCase
{
    const SENDGRID = 'sendgrid';
    const MAILJET = 'mailjet';

    /**
     * @test
     * @covers ::switch
     * @dataProvider mailDataProvider
     * @param string $currProvider
     * @param string $nextProvider
     */
    function it_should_switch_provider(string $currProvider, string $nextProvider)
    {
        $switcher = new MailProviderSwitcher();

        $this->assertEquals($nextProvider, $switcher->switch($currProvider));
    }

    /**
     * @return array
     */
    public function mailDataProvider(): array
    {
        return [
            [self::MAILJET, self::SENDGRID],
            [self::SENDGRID, self::MAILJET],
        ];
    }
}

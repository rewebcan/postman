<?php


namespace App\Http\Middleware;

use App\Enums\MailTypeEnums;
use Closure;
use Illuminate\Http\Request;
use League\CommonMark\MarkdownConverterInterface;

/**
 * Class ConvertMDToHTML
 * @package App\Http\Middleware
 */
class ConvertMarkdownContentToHTML
{
    /** @var MarkdownConverterInterface */
    private $markdownConverter;

    /**
     * ConvertMarkdownContentToHTML constructor.
     */
    public function __construct(MarkdownConverterInterface $markdownConverter)
    {
        $this->markdownConverter = $markdownConverter;
    }

    /**
     * @param Request $request
     * @param Closure $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if ($request->input('content.type') !== MailTypeEnums::MARKDOWN) {
            return $next($request);
        }

        $request->merge([
            'content' => [
                'value' => addslashes($this->markdownConverter->convertToHtml($request->input('content.value'))),
                'type' => MailTypeEnums::HTML,
            ],
        ]);

        return $next($request);
    }
}

<?php


namespace Tests\Unit\Services\CircuitBreakerManager\KeyHelper;

use App\Services\CircuitBreakerManager\KeyHelper\KeyHelper;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

/**
 * Class KeyHelperTest
 * @package Tests\Unit\Services\CircuitBreakerManager
 * @coversDefaultClass KeyHelper
 */
class KeyHelperTest extends TestCase
{
    use WithFaker;

    const OPEN = 'postman::%s::open';
    const HALF_OPEN = 'postman::%s::half_open';
    const FAILURE = 'postman::%s::failure';

    /** @var KeyHelper */
    private $keyHelper;
    /** @var string */
    private $namespace;

    /**
     * @return void;
     */
    function setUp():void
    {
        parent::setUp();

        $this->namespace = $this->faker->name;
        $this->keyHelper = new KeyHelper($this->namespace);
    }

    /**
     * @test
     * @covers ::__construct
     * @covers ::getOpenKey
     */
    function it_should_get_open_key()
    {
        $this->assertEquals(sprintf(self::OPEN, $this->namespace), $this->keyHelper->getOpenKey());
    }

    /**
     * @test
     * @covers ::getHalfOpenKey()
     */
    function it_should_get_half_open_key()
    {
        $this->assertEquals(sprintf(self::HALF_OPEN, $this->namespace), $this->keyHelper->getHalfOpenKey());
    }

    /**
     * @test
     * @covers ::getFailureKey()
     */
    function it_should_get_failure_key()
    {
        $this->assertEquals(sprintf(self::FAILURE, $this->namespace), $this->keyHelper->getFailureKey());
    }
}

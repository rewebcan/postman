<?php

namespace Tests\Unit\Http\Requests\API\V1;

use App\Http\Requests\API\V1\MailListRequest;
use Tests\TestCase;

/**
 * Class MailListRequestTest
 * @package Tests\Unit\Http\Requests\API\V1
 * @coversDefaultClass \App\Http\Requests\API\V1\MailListRequest
 */
class MailListRequestTest extends TestCase
{
    const TYPES = ['text', 'html', 'markdown'];

    /**
     * @test
     * @covers ::rules
     * @dataProvider rulesProvider
     * @param string $field
     * @param string|array $rule
     */
    function it_should_validate_rules(string $field, $rule)
    {
        $this->assertSame($rule, (new MailListRequest())->rules()[$field]);
    }

    /**
     * @test
     * @covers ::rules
     */
    function it_should_count_validation_rules()
    {
        $this->assertCount(count($this->rulesProvider()), (new MailListRequest())->rules());
    }

    /**
     * @return array
     */
    public function rulesProvider(): array
    {
        return [
            ['page', 'int'],
            ['per_page', 'int|max:50'],
        ];
    }
}

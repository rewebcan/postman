<?php


namespace App\Services\CircuitBreakerManager;

/**
 * Class CircuitBreakerSettings
 * @package App\Services\CircuitBreakerManager
 */
class CircuitBreakerSettings
{
    /** @var int */
    private $ttl = 30;
    /** @var int */
    private $halfOpenTTL = 20;
    /** @var int */
    private $maxFailureCount = 5;

    /**
     * @param mixed $ttl
     * @return CircuitBreakerSettings
     */
    public function setTTL(int $ttl): CircuitBreakerSettings
    {
        $this->ttl = $ttl;

        return $this;
    }

    /**
     * @param mixed $halfOpenTTL
     * @return CircuitBreakerSettings
     */
    public function setHalfOpenTTL(int $halfOpenTTL): CircuitBreakerSettings
    {
        $this->halfOpenTTL = $halfOpenTTL;

        return $this;
    }

    /**
     * @param int $maxFailureCount
     * @return CircuitBreakerSettings
     */
    public function setMaxFailureCount(int $maxFailureCount): CircuitBreakerSettings
    {
        $this->maxFailureCount = $maxFailureCount;

        return $this;
    }

    /**
     * @return int
     */
    public function getTTL(): int
    {
        return $this->ttl;
    }

    /**
     * @return int
     */
    public function getHalfOpenTTL(): int
    {
        return $this->halfOpenTTL;
    }

    /**
     * @return int
     */
    public function getMaxFailureCount(): int
    {
        return $this->maxFailureCount;
    }
}

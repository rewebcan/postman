[![StyleCI](https://gitlab.styleci.io/repos/26468603/shield?branch=master)](https://gitlab.styleci.io/repos/26468603?branch=master)

### Postman
It sends email using circuit breaker mechanism.

### Introduction
----------------  
I tried to implement Circuit Breaker Pattern which is Martin Fowler explains in an article [here](https://martinfowler.com/bliki/CircuitBreaker.html).

Exposed postman API basically accepts a request that we need to send an email and push it to the queue to handle mail sending and logging.  There, we are distributing our needings into events and jobs, MailSender triggers mail sending  event and its factory logic handles success or failure status with mighty Circuit Breaker. If a mail can not be sent by its provider, it requeues again into same MailSender job, untill it tries max amount of failure count.
````

                                                         +-------------+ +-------------+  +-------------+
                                                         |             | |             |  |             |
                                                         |             | |             |  |             |
                                                +------> |EmailSending | |  EmailSent  |  |EmailSending |
                                                |        |    Logging  | |             |  |       Failed|
                                                |        |             | |             |  |             |
+----------+        +------------------+        |        +-------------+ +-------------+  +-------------+
|          |        |                  |        |                                 ^               ^
|          |        |                  |        |                                 |               |
| Payload  +------> |  PostmanManager  +--------+        +--------------+         |               |
|          |        |                  |        |        |              |         |               |
|          |        |                  |        |        |              |         |               |
+----------+        +------------------+        +------> | MailSender   |         |               |
                                                +------> |    Job       |         |               |
                                                |        |              |         |               |
                                                |        +-------+------+         |               |
                                                |                |                |               |
                                                |                |                |               |
                                                |                |                |               |
                                                |                v                |               |
                                                |         +-------------+         |               |
                                                |         |             |         |               |
                                                |         |             |         |               |
                                                |         |  Circuit    +----> Success            |
                                               Fail       |    Breaking |                         |
                                                ^         |             |                         |
                                                |         +------+------+                         |
                                                |                |                                |
                                                |                |                                |
                                                |                |                                |
                                                +----------------+--------------------------------+
````

### Installation
----
First we need our service up, built and running, ( it works with m1 chips too )

You might want to add postman.test to your hosts file and make sure you are not running anything on port :80
```bash
127.0.0.1 postman.test
```

Docker Compose
```bash
docker-compose up -d
```

And then we need to access our docker container to execute our installing dependencies commands,
```bash
docker exec -it postman-fpm bash
```
Before install our dependencies, we might want to copy our .env.example to .env
```bash
cp .env-example .env  
```
At the end of .env file, we have 3 env variable, don't forget to modify it,
```bash
PRIMARY_MAIL_PROVIDER=sendgrid  
SENDGRID_SECRET_KEY=  
MAILJET_SECRET_KEY=
```
Installing dependencies,
```bash
composer install  
npm install && npm run dev
```
Migration
```bash
php artisan migrate
```
Data Seed
```bash
php artisan db:seed
```
Since we are using queues, you may have it start on supervisor
```bash
service supervisor start  
```  
Or you may just run the following command
```bash
php artisan queue:work  
```

### REST API

You can import [this postman collection](https://www.postman.com/recepcan/workspace/works/collection/9558696-20266c77-96c2-45f3-bb76-86bd5eb72b67?ctx=documentation) and use it for testing purposes

### CLI

Example CLI Command

```bash
php artisan postman:send-mail --fromName="Recep Can" --fromEmail="recep.can@useinsider.com" --replyToName="Recep Can" --replyToEmail="recep.can@useinsider.com" --recipient="John Doe:rewebcan@gmail.com" --subject="test" --content='Test'
```

### Unit tests
Run all unit test with one command
```bash
vendor/bin/phpunit 
```


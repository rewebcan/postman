<?php

namespace Tests\Unit\Listeners;

use App\Events\MailSent;
use App\Listeners\LogMailSent;
use App\Models\Mail;
use App\Repositories\Mail\MailRepositoryInterface;
use App\Values\Mail\MailPayload;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

/**
 * Class LogMailSentTest
 * @package Tests\Unit\Listeners
 * @coversDefaultClass \App\Listeners\LogMailSent
 */
class LogMailSentTest extends TestCase
{
    use WithFaker;

    const SUCCESS = 'success';

    /**
     * @test
     * @covers ::handle
     */
    function it_should_handle_mail_sent_event()
    {
        $mailPayload = new MailPayload([]);
        $provider = $this->faker->word;
        $mailModelMock = $this->createMock(Mail::class);
        $eventMock = $this->createMock(MailSent::class);
        $mailRepositoryMock = $this->createMock(MailRepositoryInterface::class);
        $listener = new LogMailSent($mailRepositoryMock);

        $mailRepositoryMock->expects($this->once())
            ->method('findOneByUUID')
            ->with($mailPayload->getUUID())
            ->willReturn($mailModelMock);
        $eventMock->expects($this->once())->method('getMailPayload')->willReturn($mailPayload);
        $eventMock->expects($this->once())->method('getMailProvider')->willReturn($provider);
        $mailModelMock->expects($this->once())->method('setStatus')->with(self::SUCCESS, $provider);

        $listener->handle($eventMock);
    }
}

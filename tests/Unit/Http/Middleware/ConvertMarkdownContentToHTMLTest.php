<?php

namespace Tests\Unit\Http\Middleware;

use App\Http\Middleware\ConvertMarkdownContentToHTML;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Request;
use League\CommonMark\MarkdownConverterInterface;
use Tests\TestCase;

/**
 * Class ConvertMarkdownContentToHTMLTest
 * @package Tests\Unit\Http\Middleware
 * @coversDefaultClass \App\Http\Middleware\ConvertMarkdownContentToHTML
 */
class ConvertMarkdownContentToHTMLTest extends TestCase
{
    use WithFaker;

    /**
     * @test
     * @covers ::__construct
     * @covers ::handle
     */
    function it_should_convert_md_to_html()
    {
        $contentType = 'markdown';
        $nextContentType = 'html';
        $markdownContent = <<<EOD
            # Heading level 1
            Paragraph __bold text__
        EOD;
        $htmlContent = '<h1>Heading level 1</h1><p>Paragraph <strong>bold text</strong></p>';
        $request = $this->createMock(Request::class);
        $markdownConverterMock = $this->createMock(MarkdownConverterInterface::class);
        $middleware = new ConvertMarkdownContentToHTML($markdownConverterMock);

        $request->expects($this->atLeastOnce())
            ->method('input')
            ->withConsecutive(['content.type'], ['content.value'])
            ->willReturnOnConsecutiveCalls($contentType, $markdownContent);
        $request->expects($this->once())
            ->method('merge')
            ->with([
                'content' => [
                    'value' => addslashes($htmlContent),
                    'type' => $nextContentType,
                ],
            ]);
        $markdownConverterMock->expects($this->once())
            ->method('convertToHtml')
            ->with($markdownContent)
            ->willReturn($htmlContent);

        $middleware->handle($request, function (Request $nextRequest) use ($request) {
            $this->assertEquals($request, $nextRequest);
        });
    }
}

<?php

namespace Tests\Unit\Listeners;

use App\Events\MailSending;
use App\Listeners\LogSendingMail;
use App\Repositories\Mail\MailRepositoryInterface;
use App\Values\Mail\MailPayload;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

/**
 * Class LogSendingMailTest
 * @package Tests\Unit\Listeners
 * @coversDefaultClass \App\Listeners\LogSendingMail
 */
class LogSendingMailTest extends TestCase
{
    use WithFaker;

    const SENDING = 'sending';

    /**
     * @test
     * @covers ::handle
     */
    function it_should_handle_mail_sending_event()
    {
        $subject = $this->faker->sentence;
        $type = $this->faker->word;
        $contentValue = $this->faker->text;

        $payload = [
            'subject' => $subject,
            'content' => ['type' => $type, 'value' => $contentValue],
        ];
        $mailPayload = new MailPayload($payload);
        $eventMock = $this->createMock(MailSending::class);
        $mailRepositoryMock = $this->createMock(MailRepositoryInterface::class);

        $eventMock->expects($this->any())->method('getEmailPayload')->willReturn($mailPayload);
        $mailRepositoryMock->expects($this->once())->method('create')->with([
            'uuid' => $mailPayload->getUUID(),
            'subject' => $subject,
            'type' => $type,
            'content' => $contentValue,
            'status' => self::SENDING,
        ]);

        (new LogSendingMail($mailRepositoryMock))->handle($eventMock);
    }
}

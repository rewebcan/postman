<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class MailResource
 * @property mixed mail_provider
 * @property mixed status
 * @property mixed content
 * @property mixed type
 * @property mixed subject
 * @property mixed uuid
 * @property mixed id
 * @package App\Http\Resources
 */
class MailResource extends JsonResource
{
    /**
     * @param Request $request
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'uuid' => $this->uuid,
            'subject' => $this->subject,
            'type' => $this->type,
            'content' => $this->content,
            'status' => $this->status,
            'mail_provider' => $this->mail_provider,
        ];
    }
}

<?php

namespace Tests\Unit\Services\MailProviders;

use App\Services\MailProviders\SendgridProvider;
use App\Values\Mail\MailPayload;
use GuzzleHttp\Client;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

/**
 * Class SendgridProviderTest
 * @package Tests\Unit\Services\MailProviders
 * @coversDefaultClass \App\Services\MailProviders\SendgridProvider
 */
class SendgridProviderTest extends TestCase
{
    use WithFaker;

    const TEXT = 'text';
    const HTML = 'html';

    /**
     * @test
     * @covers ::__construct
     * @covers ::make
     * @throws \Throwable
     */
    function it_should_send_request()
    {
        $payload = new MailPayload([
            'subject' => $this->faker->word,
            'from' => ['name' => $this->faker->name, 'email' => $this->faker->email],
            'recipients' => [['name' => $this->faker->name, 'email' => $this->faker->email]],
            'replyTo' => ['name' => $this->faker->name, 'email' => $this->faker->email],
            'content' => [
                'type' => $this->faker->randomElement([self::TEXT, self::HTML]),
                'value' => $this->faker->sentence,
            ],
        ]);
        $clientMock = $this->createMock(Client::class);

        $clientMock->expects($this->once())->method('sendRequest')->withAnyParameters();

        (new SendgridProvider($clientMock))->send($payload);
    }
}

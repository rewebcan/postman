<?php


namespace Tests\Unit\Services\CircuitBreakerManager;

use App\Services\CircuitBreakerManager\Adapter\CircuitBreakerAdapterInterface;
use App\Services\CircuitBreakerManager\CircuitBreakerManager;
use App\Services\CircuitBreakerManager\CircuitBreakerSettings;
use Tests\TestCase;

/**
 * Class CircuitBreakerManagerTest
 * @package Tests\Unit\Services\CircuitBreakerManager
 * @coversDefaultClass \App\Services\CircuitBreakerManager\CircuitBreakerManager
 */
class CircuitBreakerManagerTest extends TestCase
{
    /** @var CircuitBreakerAdapterInterface */
    private $circuitBreakerAdapterMock;
    /** @var CircuitBreakerSettings */
    private $circuitBreakerConfig;

    protected function setUp(): void
    {
        parent::setUp();

        $this->circuitBreakerAdapterMock = $this->createMock(CircuitBreakerAdapterInterface::class);
        $this->circuitBreakerConfig = new CircuitBreakerSettings();
    }

    /**
     * @test
     * @covers ::__construct
     */
    function it_should_configure_construct_with_default_settings()
    {
        $circuitBreakerManager = new CircuitBreakerManager($this->circuitBreakerAdapterMock);

        $this->assertProperty($circuitBreakerManager, 'settings', $this->circuitBreakerConfig);
    }

    /**
     * @test
     * @covers ::configureSettings
     * @throws \ReflectionException
     */
    function it_should_configure_construct_with_given_settings()
    {
        $circuitBreakerManager = new CircuitBreakerManager($this->circuitBreakerAdapterMock);
        $circuitBreakerConfig = new CircuitBreakerSettings();
        $circuitBreakerManager->configureSettings($circuitBreakerConfig);

        $this->assertProperty($circuitBreakerManager, 'settings', $circuitBreakerConfig);
    }

    /**
     * @test
     * @covers ::isAvailable
     */
    function it_should_return_false_when_circuit_is_open()
    {
        $this->circuitBreakerAdapterMock->expects($this->once())->method('isOpen')->willReturn(true);

        $circuitBreakerManager = new CircuitBreakerManager($this->circuitBreakerAdapterMock);

        $this->assertFalse($circuitBreakerManager->isAvailable());
    }

    /**
     * @test
     * @covers ::isAvailable
     */
    function it_should_set_open_and_set_half_open_circuit_when_reach_limit()
    {
        $this->circuitBreakerAdapterMock->expects($this->once())->method('isOpen')->willReturn(false);
        $this->circuitBreakerAdapterMock->expects($this->once())
            ->method('reachRateLimit')
            ->with($this->circuitBreakerConfig->getMaxFailureCount())
            ->willReturn(true);
        $this->circuitBreakerAdapterMock->expects($this->once())
            ->method('setOpen')
            ->with($this->circuitBreakerConfig->getTTL());
        $this->circuitBreakerAdapterMock->expects($this->once())
            ->method('setHalfOpen')
            ->with($this->circuitBreakerConfig->getTTL(), $this->circuitBreakerConfig->getHalfOpenTTL());

        $circuitBreakerManager = new CircuitBreakerManager($this->circuitBreakerAdapterMock);

        $this->assertFalse($circuitBreakerManager->isAvailable());
    }

    /**
     * @test
     * @covers ::isAvailable
     */
    function it_should_return_true_when_service_is_available()
    {
        $this->circuitBreakerAdapterMock->expects($this->once())->method('isOpen')->willReturn(false);
        $this->circuitBreakerAdapterMock->expects($this->once())
            ->method('reachRateLimit')
            ->with($this->circuitBreakerConfig->getMaxFailureCount())
            ->willReturn(false);

        $circuitBreakerManager = new CircuitBreakerManager($this->circuitBreakerAdapterMock);

        $this->assertTrue($circuitBreakerManager->isAvailable());
    }

    /**
     * @test
     * @covers ::failed
     */
    function it_should_set_open_and_set_half_open_circuit_when_is_half_open()
    {
        $this->circuitBreakerAdapterMock->expects($this->once())
            ->method('isHalfOpen')
            ->willReturn(true);
        $this->circuitBreakerAdapterMock->expects($this->once())
            ->method('setOpen')
            ->with($this->circuitBreakerConfig->getTTL());
        $this->circuitBreakerAdapterMock->expects($this->once())
            ->method('setHalfOpen');

        $circuitBreakerManager = new CircuitBreakerManager($this->circuitBreakerAdapterMock);

        $this->assertFalse($circuitBreakerManager->failed());
    }

    /**
     * @test
     * @covers ::failed
     */
    function it_should_increment_failure_when_circuit_is_not_half_open()
    {
        $this->circuitBreakerAdapterMock->expects($this->once())
            ->method('isHalfOpen')
            ->willReturn(false);
        $this->circuitBreakerAdapterMock->expects($this->once())
            ->method('incrementFailure')
            ->with($this->circuitBreakerConfig->getTTL())
            ->willReturn(true);

        $circuitBreakerManager = new CircuitBreakerManager($this->circuitBreakerAdapterMock);

        $this->assertTrue($circuitBreakerManager->failed());
    }

    /**
     * @test
     * @covers ::success
     */
    function it_should_set_success()
    {
        $this->circuitBreakerAdapterMock->expects($this->once())
            ->method('setSuccess');

        (new CircuitBreakerManager($this->circuitBreakerAdapterMock))->success();
    }
}

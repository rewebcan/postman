<?php

namespace Tests\Feature\Controllers\API\V1;

use App\Http\Controllers\API\V1\PostmanController;
use App\Jobs\PostmanManager;
use App\Values\Mail\MailPayload;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Queue;
use Tests\TestCase;

/**
 * Class PostmanControllerTest
 * @package Tests\Feature\Controllers\API\V1
 * @coversDefaultClass PostmanController
 */
class PostmanControllerTest extends TestCase
{
    use DatabaseMigrations, WithFaker;

    const PRIMARY_MAIL_PROVIDER = 'sendgrid';
    const TEXT = 'text';
    const HTML = 'html';

    /**
     * @test
     * @covers ::store
     * @return void
     */
    function it_should_send_new_email()
    {
        Queue::fake();

        $requestBody = [
            'subject' => $this->faker->word,
            'from' => ['name' => $this->faker->name, 'email' => $this->faker->email],
            'recipients' => [['name' => $this->faker->name, 'email' => $this->faker->email]],
            'replyTo' => ['name' => $this->faker->name, 'email' => $this->faker->email],
            'content' => [
                'type' => $this->faker->randomElement([self::TEXT, self::HTML]),
                'value' => $this->faker->sentence,
            ],
        ];
        $mailPayload = new MailPayload($requestBody);
        $responseBody = $this->post(route('send-email'), $requestBody);
        $responseBody->assertOk()->assertSee('ok');

        Queue::assertPushed(PostmanManager::class, function (PostmanManager $manager) use ($mailPayload, $requestBody) {
            $this->assertProperty($manager, 'mailProvider', self::PRIMARY_MAIL_PROVIDER);
            $this->assertEquals($mailPayload->getSubject(), $requestBody['subject']);

            return true;
        });
    }

    /**
     * @test
     * @covers ::index
     */
    function it_should_index_mails()
    {
        $requestBody = [
            'per_page' => 5,
            'page' => 1,
        ];

        $responseBody = $this->get(route('get-emails'));

        $responseBody->assertOk()->assertSee('per_page');
    }
}

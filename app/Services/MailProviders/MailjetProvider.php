<?php


namespace App\Services\MailProviders;

use App\Enums\MailTypeEnums;
use App\Values\Mail\MailAddress;
use App\Values\Mail\MailPayload;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;
use Psr\Http\Message\ResponseInterface;
use Throwable;

/**
 * Class MailjetProvider
 * @package App\Services\MailProviders
 */
class MailjetProvider implements MailProviderInterface
{
    /** @var Client */
    private $httpClient;

    public function __construct(Client $httpClient)
    {
        $this->httpClient = $httpClient;
    }

    /**
     * @param MailPayload $payload
     * @throws Throwable
     * @return ResponseInterface
     */
    public function send(MailPayload $payload): ResponseInterface
    {
        $method = 'POST';
        $url = config('services.mailjet.endpoints.send_email');
        $apiToken = config('services.mailjet.secret');
        $headers = [
            'Authorization' => "Basic $apiToken",
            'Content-Type' => 'application/json',
        ];
        $recipients = array_map(function (MailAddress $mailAddress) {
            return ['Name' => $mailAddress->getFullName(), 'Email' => $mailAddress->getEmailAddress()];
        }, $payload->getRecipients());
        $textContent = '';
        $htmlContent = '';

        if ($payload->getContentType() === MailTypeEnums::TEXT) {
            $textContent = $payload->getContent();
        } else {
            $htmlContent = $payload->getContent();
        }

        $requestBody = collect([
            'Messages' => [
                [
                    'From' => [
                        'Name' => $payload->getFrom()->getFullName(),
                        'Email' => $payload->getFrom()->getEmailAddress(),
                    ],
                    'To' => $recipients,
                    'Subject' => $payload->getSubject(),
                    'TextPart' => $textContent,
                    'HTMLPart' => $htmlContent,
                    'CustomID' => $payload->getUUID(),
                ],
            ],
        ]);

        return $this->httpClient->sendRequest(new Request($method, $url, $headers, $requestBody->toJson()));
    }
}

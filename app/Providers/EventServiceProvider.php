<?php

namespace App\Providers;

use App\Events\MailSending;
use App\Events\MailSendingFailed;
use App\Events\MailSent;
use App\Listeners\LogMailSendingFailed;
use App\Listeners\LogMailSent;
use App\Listeners\LogSendingMail;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
        MailSending::class => [
            LogSendingMail::class,
        ],
        MailSent::class => [
            LogMailSent::class,
        ],
        MailSendingFailed::class => [
            LogMailSendingFailed::class,
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}

<?php

namespace App\Listeners;

use App\Enums\MailStatusEnums;
use App\Events\MailSending;
use App\Repositories\Mail\MailRepositoryInterface;

/**
 * Class LogSendingMail
 * @package App\Listeners
 */
class LogSendingMail
{
    /**
     * @var MailRepositoryInterface
     */
    private $mailRepository;

    public function __construct(MailRepositoryInterface $mailRepository)
    {
        $this->mailRepository = $mailRepository;
    }

    /**
     * @param MailSending $event
     * @return void
     */
    public function handle(MailSending $event)
    {
        $this->mailRepository->create([
            'uuid' => $event->getEmailPayload()->getUUID(),
            'subject' => $event->getEmailPayload()->getSubject(),
            'type' => $event->getEmailPayload()->getContentType(),
            'content' => $event->getEmailPayload()->getContent(),
            'status' => MailStatusEnums::SENDING,
        ]);
    }
}

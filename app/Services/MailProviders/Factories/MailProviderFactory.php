<?php


namespace App\Services\MailProviders\Factories;

use App\Enums\MailProviderEnums;
use App\Services\MailProviders\MailjetProvider;
use App\Services\MailProviders\MailProviderInterface;
use App\Services\MailProviders\SendgridProvider;
use GuzzleHttp\Client;
use InvalidArgumentException;

/**
 * Class MailProviderFactory
 * @package App\Services\MailProviders
 */
class MailProviderFactory
{
    /** @var Client */
    private $httpClient;

    /**
     * MailProviderFactory constructor.
     * @param Client $httpClient
     */
    public function __construct(Client $httpClient)
    {
        $this->httpClient = $httpClient;
    }

    /**
     * @param string $provider
     * @return MailProviderInterface
     */
    public function make(string $provider): MailProviderInterface
    {
        switch ($provider) {
            case MailProviderEnums::SENDGRID:
                return new SendgridProvider($this->httpClient);
            case MailProviderEnums::MAILJET:
                return new MailjetProvider($this->httpClient);
            default:
                throw new InvalidArgumentException("$provider does not supported");
        }
    }
}

<?php

namespace App\Jobs;

use App\Events\MailSendingFailed;
use App\Events\MailSent;
use App\Services\CircuitBreakerManager\Factories\CircuitBreakerManagerFactory;
use App\Services\MailProviders\Factories\MailProviderFactory;
use App\Services\MailProviders\Utilities\MailProviderSwitcher;
use App\Values\Mail\MailPayload;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Queue;

/**
 * Class MailSender
 * @package App\Jobs
 */
class MailSender implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable;

    const QUEUE_DELAY = 5;

    /** @var string */
    private $mailProvider;
    /** @var array */
    private $mailPayload;

    /**
     * @param string $mailProvider
     * @param MailPayload $mailPayload
     */
    public function __construct(string $mailProvider, MailPayload $mailPayload)
    {
        $this->mailProvider = $mailProvider;
        $this->mailPayload = $mailPayload;
    }

    /**
     * @param CircuitBreakerManagerFactory $cbmFactory
     * @param MailProviderFactory $mailProviderFactory
     * @param MailProviderSwitcher $mailProviderSwitcher
     * @return void
     */
    public function handle(
        CircuitBreakerManagerFactory $cbmFactory,
        MailProviderFactory $mailProviderFactory,
        MailProviderSwitcher $mailProviderSwitcher
    ): void {
        $circuitBreakerManager = $cbmFactory->make($this->mailProvider);
        $mailProvider = $mailProviderFactory->make($this->mailProvider);

        if (!$circuitBreakerManager->isAvailable()) {
            $nextProvider = $mailProviderSwitcher->switch($this->mailProvider);
            Queue::push(new self($nextProvider, $this->mailPayload));

            return;
        }

        try {
            $mailProvider->send($this->mailPayload);
            $circuitBreakerManager->success();

            event(new MailSent($this->mailProvider, $this->mailPayload));
        } catch (\Exception $exception) {
            if ($circuitBreakerManager->failed()) {
                $nextProvider = $mailProviderSwitcher->switch($this->mailProvider);

                event(new MailSendingFailed($nextProvider, $this->mailPayload));
                Queue::later(self::QUEUE_DELAY, new self($nextProvider, $this->mailPayload));
            }
        }
    }
}

<?php

namespace Tests\Unit\Jobs;

use App\Events\MailSendingFailed;
use App\Events\MailSent;
use App\Jobs\MailSender;
use App\Services\CircuitBreakerManager\CircuitBreakerManager;
use App\Services\CircuitBreakerManager\Factories\CircuitBreakerManagerFactory;
use App\Services\MailProviders\Factories\MailProviderFactory;
use App\Services\MailProviders\MailProviderInterface;
use App\Services\MailProviders\Utilities\MailProviderSwitcher;
use App\Values\Mail\MailPayload;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Queue;
use Mockery;
use Tests\TestCase;

/**
 * Class MailSenderTest
 * @package Tests\Unit\Jobs
 * @coversDefaultClass \App\Jobs\MailSender
 */
class MailSenderTest extends TestCase
{
    use WithFaker;

    const TEXT = 'text';
    const HTML = 'html';
    const QUEUE_DELAY = '5';

    /** @var CircuitBreakerManagerFactory */
    private $circuitBreakerManagerFactoryMock;
    /** @var MailProviderFactory */
    private $mailProviderFactoryMock;
    /** @var MailProviderSwitcher */
    private $mailProviderSwitcherMock;
    /** @var string */
    private $currentProviderName;
    /** @var MailPayload */
    private $mailPayload;
    /** @var MailSender */
    private $mailSender;

    /**
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();

        $this->circuitBreakerManagerFactoryMock = $this->createMock(CircuitBreakerManagerFactory::class);
        $this->mailProviderFactoryMock = $this->createMock(MailProviderFactory::class);
        $this->mailProviderSwitcherMock = $this->createMock(MailProviderSwitcher::class);
        $this->currentProviderName = $this->faker->name;
        $this->mailPayload = new MailPayload([
            'subject' => $this->faker->sentence,
            'from' => ['name' => $this->faker->name, 'email' => $this->faker->email],
            'recipients' => [['name' => $this->faker->name, 'email' => $this->faker->email]],
            'replyTo' => ['name' => $this->faker->name, 'email' => $this->faker->email],
            'content' => [
                'type' => $this->faker->randomElement([self::TEXT, self::HTML]),
                'content' => $this->faker->sentence,
            ],
        ]);
        $this->mailSender = new MailSender($this->currentProviderName, $this->mailPayload);
    }

    /**
     * @test
     * @covers ::__construct
     * @covers ::handle
     */
    function it_should_push_mail_sender_job_when_service_is_unavailable()
    {
        $this->withoutExceptionHandling();

        Queue::fake();
        Event::fake();

        $nextProviderName = $this->faker->name;

        $circuitBreakerManagerMock = $this->createMock(CircuitBreakerManager::class);
        $mailProviderMock = $this->createMock(MailProviderInterface::class);

        $this->circuitBreakerManagerFactoryMock->expects($this->once())
            ->method('make')
            ->with($this->currentProviderName)
            ->willReturn($circuitBreakerManagerMock);
        $this->mailProviderFactoryMock->expects($this->once())
            ->method('make')
            ->with($this->currentProviderName)
            ->willReturn($mailProviderMock);
        $circuitBreakerManagerMock->expects($this->once())
            ->method('isAvailable')
            ->willReturn(false);
        $this->mailProviderSwitcherMock->expects($this->once())
            ->method('switch')
            ->with($this->currentProviderName)
            ->willReturn($nextProviderName);

        $this->mailSender->handle(
            $this->circuitBreakerManagerFactoryMock,
            $this->mailProviderFactoryMock,
            $this->mailProviderSwitcherMock
        );

        Queue::assertPushed(MailSender::class, function (MailSender $mailSender) use ($nextProviderName) {
            $this->assertProperty($mailSender, 'mailProvider', $nextProviderName);
            $this->assertProperty($mailSender, 'mailPayload', $this->mailPayload);

            return true;
        });

        Event::assertNotDispatched(MailSent::class);
        Event::assertNotDispatched(MailSendingFailed::class);
    }

    /**
     * @test
     * @covers ::handle
     */
    function it_should_trigger_mail_sent_event_when_everything_is_ok()
    {
        $this->withoutExceptionHandling();

        Queue::fake();
        Event::fake();

        $circuitBreakerManagerMock = $this->createMock(CircuitBreakerManager::class);
        $mailProviderMock = $this->createMock(MailProviderInterface::class);

        $this->circuitBreakerManagerFactoryMock->expects($this->once())
            ->method('make')
            ->with($this->currentProviderName)
            ->willReturn($circuitBreakerManagerMock);
        $this->mailProviderFactoryMock->expects($this->once())
            ->method('make')
            ->with($this->currentProviderName)
            ->willReturn($mailProviderMock);
        $circuitBreakerManagerMock->expects($this->once())->method('isAvailable')->willReturn(true);
        $mailProviderMock->expects($this->once())->method('send');
        $circuitBreakerManagerMock->expects($this->once())->method('success');

        $this->mailSender->handle(
            $this->circuitBreakerManagerFactoryMock,
            $this->mailProviderFactoryMock,
            $this->mailProviderSwitcherMock
        );

        Event::assertDispatched(MailSent::class, function (MailSent $mailSent) {
            $this->assertProperty($mailSent, 'mailProvider', $this->currentProviderName);
            $this->assertProperty($mailSent, 'mailPayload', $this->mailPayload);

            return true;
        });

        Queue::assertNothingPushed();
    }

    /**
     * @test
     * @covers ::handle
     */
    function it_should_try_later_on_mail_sender_job_when_an_exception_thrown_by_send()
    {
        $this->withoutExceptionHandling();

        Queue::fake();
        Event::fake();

        $nextProviderName = $this->faker->name;

        $circuitBreakerManagerMock = $this->createMock(CircuitBreakerManager::class);
        $mailProviderMock = $this->createMock(MailProviderInterface::class);

        $this->circuitBreakerManagerFactoryMock->expects($this->once())
            ->method('make')
            ->with($this->currentProviderName)
            ->willReturn($circuitBreakerManagerMock);
        $this->mailProviderFactoryMock->expects($this->once())
            ->method('make')
            ->with($this->currentProviderName)
            ->willReturn($mailProviderMock);
        $circuitBreakerManagerMock->expects($this->once())->method('isAvailable')->willReturn(true);
        $mailProviderMock->expects($this->once())->method('send');
        $circuitBreakerManagerMock->expects($this->once())
            ->method('success')
            ->willThrowException(new \Exception('fake exception'));
        $circuitBreakerManagerMock->expects($this->once())->method('failed')->willreturn(true);
        $this->mailProviderSwitcherMock->expects($this->once())
            ->method('switch')
            ->with($this->currentProviderName)
            ->willReturn($nextProviderName);

        Queue::shouldReceive('later')->once()->with(self::QUEUE_DELAY, Mockery::type(MailSender::class));

        $this->mailSender->handle(
            $this->circuitBreakerManagerFactoryMock,
            $this->mailProviderFactoryMock,
            $this->mailProviderSwitcherMock
        );

        Event::assertDispatched(
            MailSendingFailed::class,
            function (MailSendingFailed $mailSendingFailed) use ($nextProviderName) {
                $this->assertProperty($mailSendingFailed, 'mailProvider', $nextProviderName);
                $this->assertProperty($mailSendingFailed, 'mailPayload', $this->mailPayload);

                return true;
            }
        );
        Event::assertNotDispatched(MailSent::class);
    }
}

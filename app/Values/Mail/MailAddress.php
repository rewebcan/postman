<?php

namespace App\Values\Mail;

use Illuminate\Contracts\Support\Arrayable;

/**
 * Class Recipient
 * @package App\Values\Mail
 */
final class MailAddress implements Arrayable
{
    /** @var string */
    private $fullName;
    /** @var string */
    private $emailAddress;

    /**
     * Recipient constructor.
     * @param string $fullName
     * @param string $emailAddress
     */
    public function __construct(string $fullName, string $emailAddress)
    {
        $this->fullName = $fullName;
        $this->emailAddress = $emailAddress;
    }

    /**
     * @return string
     */
    public function getFullName(): string
    {
        return $this->fullName;
    }

    /**
     * @return string
     */
    public function getEmailAddress(): string
    {
        return $this->emailAddress;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return ['name' => $this->getFullName(), 'email' => $this->getEmailAddress()];
    }
}

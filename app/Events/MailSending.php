<?php

namespace App\Events;

use App\Values\Mail\MailPayload;

class MailSending
{
    /** @var array */
    private $emailPayload;

    /**
     * @param MailPayload $emailPayload
     */
    public function __construct(MailPayload $emailPayload)
    {
        $this->emailPayload = $emailPayload;
    }

    /**
     * @return MailPayload
     */
    public function getEmailPayload(): MailPayload
    {
        return $this->emailPayload;
    }
}
